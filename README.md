# README #

## General Information ##

**Authors:** Loapu

[![Maven metadata URI](https://img.shields.io/maven-metadata/v/https/repo.medievalsuite.de/repository/medievalsuite/de/medievalcraft/medievaldata/MedievalData/maven-metadata.xml.svg?style=flat-square)]()
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/medievalcraft/medievaldata.svg?style=flat-square)]()
[![License](https://img.shields.io/badge/license-GPL_v3-green.svg?style=flat-square)]()

This module is part of the MedievalSuite.
See (https://www.medievalsuite.de) for more information.

## Branches Explained ##

**development** - Contains the latest version of the module

**master** - Contains the latest feature-ready version of the module

**release-*** - Contains the mentioned release version of the module

## Need Help? ##

**Raise an issue:** https://www.medievalsuite.de/issues/daniel

**Documentation:** https://www.medievalsuite.de/docs/daniel

**JavaDocs:** https://www.medievalsuite.de/javadocs/MedievalData