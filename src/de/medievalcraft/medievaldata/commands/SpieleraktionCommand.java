/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata.commands;

import de.medievalcraft.medievaldata.MedievalData;
import de.medievalcraft.medievaldata.MedievalPerms;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpieleraktionCommand implements CommandExecutor
{
	private MedievalData daniel = MedievalData.get();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		
		if (!sender.hasPermission(MedievalPerms.SPIELERAKTION.getPerm()))
		{
			daniel.sendMessage(sender, daniel.getPrefix() + "&cDafür fehlen dir die Berechtigungen");
			return true;
		}
		
		if (args.length < 2)
		{
			daniel.sendMessage(sender, daniel.getPrefix() + "&cBitte gib mehr Argumente an");
			return true;
		}
		
		String id = args[0];
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 1; i < args.length; i++)
		{
			sb.append(args[i]).append(" ");
		}
		
		daniel.addActionLogEntry(sender.getName(), id, sb.toString());
		
		return true;
	}
}
