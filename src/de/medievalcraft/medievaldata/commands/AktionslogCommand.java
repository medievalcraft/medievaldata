/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata.commands;

import de.medievalcraft.medievaldata.MedievalData;
import de.medievalcraft.medievaldata.MedievalPerms;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AktionslogCommand implements CommandExecutor
{
	private MedievalData daniel = MedievalData.get();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		Player p = null;
		
		if (sender instanceof Player)
		{
			p = (Player) sender;
		}
		
		if (!sender.hasPermission(MedievalPerms.SPIELERINFO.getPerm()))
		{
			daniel.sendMessage(sender, daniel.getPrefix() + "&cDafür fehlen dir die Berechtigungen");
			return true;
		}
		
		if (args.length < 2)
		{
			daniel.sendMessage(sender, daniel.getPrefix() + "&cBitte gib mehr Argumente an");
			return true;
		}
		
		daniel.sendMessage(sender, daniel.getPrefix() + "Aktionslog wird generiert...");
		daniel.createUserLogPaste(p, args[0], args[1]);
		
		return true;
	}
}
