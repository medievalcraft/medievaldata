/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata.listeners;

import de.medievalcraft.medievaldata.MedievalData;
import de.medievalcraft.medievaldata.MedievalUserMeta;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class JoinListener implements Listener
{
	private MedievalData daniel = MedievalData.get();
	private MedievalUserMeta meta = new MedievalUserMeta();
	
	public JoinListener(MedievalData plugin)
	{
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();
		daniel.finalizeUser(p);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(PlayerJoinEvent e)
	{
		daniel.log("Spieler verbindet. Daten werden gelesen...");
		
		String playerName = e.getPlayer().getName();
		UUID playerUUID = e.getPlayer().getUniqueId();
		int playerID = MedievalData.getID(playerName);
		
		if (playerID != 0)
		{
			daniel.log("Spieler gefunden. Datenbankeintrag wird aktualisiert...");
			daniel.updateUser(playerID);
			return;
		}
		
		daniel.log("Spielername unbekannt. Überprüfe auf Namensänderung...");
		
		if (daniel.validateUUID(playerUUID))
		{
			daniel.log("Spieler gefunden. Namenshistorie wird aktualisiert...");
			daniel.updateName(playerUUID, playerName);
			return;
		}
		
		daniel.log("Kein Hinweis auf Namensänderung gefunden. Spieleraccount wird initialisiert...");
		
		daniel.createUser(playerUUID, playerName, true);
	}
}
