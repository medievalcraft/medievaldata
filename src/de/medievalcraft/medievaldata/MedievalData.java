/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata;

import com.google.common.collect.ImmutableList;
import de.medievalcraft.medievaldata.commands.AktionslogCommand;
import de.medievalcraft.medievaldata.commands.MedievaldataCommand;
import de.medievalcraft.medievaldata.commands.SpieleraktionCommand;
import de.medievalcraft.medievaldata.commands.SpielerinfoCommand;
import de.medievalcraft.medievaldata.listeners.JoinListener;
import de.medievalcraft.medievaldata.utils.Gist;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MedievalData extends MedievalPlugin
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private YamlConfiguration config;
	private File f;
	private String prefix = "&8[&6" + this.getDescription().getPrefix() + "&8] &7";
	private MedievalUserMeta meta = null;
	private String host, database, username, password;
	private int port;
	private HashMap<Integer, String[]> playerDataStorage = new HashMap<>(); //0 = ID, 1 = UUID, 2 = Name, 3 = History, 4 = FirstJoin, 5 = LastJoin
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd. MMMM yyyy HH:mm z", Locale.GERMANY);
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static MedievalData i;
	
	public static MedievalData get()
	{
		return i;
	}
	
	public MedievalData()
	{
		i = this;
	}
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnableInner()
	{
		config();
		loadListeners();
		registerCommands();
		meta = new MedievalUserMeta();
		
		host = config.getString("database.host");
		database = config.getString("database.name");
		username = config.getString("database.username");
		password = config.getString("database.password");
		port = config.getInt("database.port");
		
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement statement = Objects.requireNonNull(connection).createStatement();
					
					statement.execute("CREATE TABLE IF NOT EXISTS " + database + ".data_mc_accounts ( uuid VARCHAR(36) NOT NULL , playername VARCHAR(16) NOT NULL , playername_history LONGTEXT NOT NULL , PRIMARY KEY (uuid))");
					statement.execute("CREATE TABLE IF NOT EXISTS " + database + ".data_medieval_accounts ( medieval_id SERIAL NOT NULL , medieval_name VARCHAR(16) NOT NULL , last_join DATETIME NOT NULL , first_join DATETIME NOT NULL ,  action_history LONGTEXT NOT NULL , PRIMARY KEY (medieval_id))");
					statement.execute("CREATE TABLE IF NOT EXISTS " + database + ".data_link ( medieval_id SERIAL NOT NULL , uuid VARCHAR(36) NOT NULL , main BOOLEAN NOT NULL DEFAULT FALSE , PRIMARY KEY (medieval_id, uuid))");
					statement.close();
					connection.close();
				}
				catch (ClassNotFoundException | SQLException e)
				{
					e.printStackTrace();
				}
			}
		}.runTaskAsynchronously(get());
		
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				updatePlayers();
			}
		}.runTaskLater(get(), 40);
	}
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getPrefix()
	{
		return prefix;
	}
	
	public static UUID getUUID(int id)
	{
		if (!get().playerDataStorage.containsKey(id))
		{
			return null;
		}
		
		return UUID.fromString(get().playerDataStorage.get(id)[1]);
	}
	
	public static int getID(String name)
	{
		
		String target = name;
		
		if (Bukkit.getPlayer(name) != null)
		{
			target = Bukkit.getPlayer(target).getName();
		}
		
		for (Entry<Integer, String[]> e : get().playerDataStorage.entrySet())
		{
			if (e.getValue()[2].equalsIgnoreCase(target))
			{
				return Integer.valueOf(e.getValue()[0]);
			}
		}
		
		try
		{
			Connection connection = get().openConnection();
			Statement st = Objects.requireNonNull(connection).createStatement();
			
			String playerName = null;
			String uuid = null;
			String id = null;
			String lastJoin = null;
			String firstJoin = null;
			String historyString = null;
			
			
			ResultSet rs = st.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_name='" + target + "';");
			
			while (rs.next())
			{
				playerName = rs.getString("medieval_name");
				id = rs.getString("medieval_id");
				lastJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("last_join")) + " Uhr";
				firstJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("first_join")) + " Uhr";
			}
			
			rs.close();
			
			ResultSet rs2 = st.executeQuery("SELECT * FROM data_link WHERE medieval_id='" + id + "';");
			
			while (rs2.next())
			{
				uuid = rs2.getString("uuid");
			}
			
			rs2.close();
			
			ResultSet rs3 = st.executeQuery("SELECT * FROM data_mc_accounts WHERE uuid='" + uuid + "'");
			
			while (rs3.next())
			{
				historyString = rs3.getString("playername_history");
			}
			rs3.close();
			st.close();
			connection.close();
			
			if (id == null)
			{
				return 0;
			}
			
			String[] dataArray = {id, uuid, playerName, historyString, lastJoin, firstJoin};
			
			get().playerDataStorage.put(Integer.valueOf(id), dataArray);
			return Integer.valueOf(id);
		}
		catch (ClassNotFoundException | SQLException e)
		{
			get().logError(e);
			return 0;
		}
	}
	
	public static String getName(int id)
	{
		if (!get().playerDataStorage.containsKey(id))
		{
			return null;
		}
		
		return get().playerDataStorage.get(id)[2];
	}
	
	public static String[] getPlayernameHistory(int id)
	{
		if (!get().playerDataStorage.containsKey(id))
		{
			return null;
		}
		
		String historyString = get().playerDataStorage.get(id)[3];
		String historyStringMinusBrackets = historyString.substring(1, historyString.length() - 1);
		
		return historyStringMinusBrackets.split(", ");
	}
	
	public static String getLastJoinDate(int id)
	{
		if (!get().playerDataStorage.containsKey(id))
		{
			return null;
		}
		
		return get().playerDataStorage.get(id)[5];
	}
	
	public static String getFirstJoinDate(int id)
	{
		if (!get().playerDataStorage.containsKey(id))
		{
			return null;
		}
		
		return get().playerDataStorage.get(id)[4];
	}
	
	// -------------------------------------------- //
	// METHODS
	// -------------------------------------------- //
	
	private void loadListeners()
	{
		new JoinListener(get());
		
		if (config.getBoolean("enable auto updater"))
		{
			MedievalUpdater updater = new MedievalUpdater();
			Bukkit.getPluginManager().registerEvents(updater, this);
		}
	}
	
	private void registerCommands()
	{
		MedievaldataCommand mc = new MedievaldataCommand(get());
		getCommand("medievaldata").setExecutor(mc);
		
		//AktionslogCommand ac = new AktionslogCommand();
		//getCommand("aktionslog").setExecutor(ac);
		
		SpielerinfoCommand sc = new SpielerinfoCommand();
		getCommand("spielerinfo").setExecutor(sc);
		
		//SpieleraktionCommand sac = new SpieleraktionCommand();
		//getCommand("spieleraktion").setExecutor(sac);
		
	}
	
	public boolean validateUUID(UUID uuid)
	{
		try
		{
			Connection connection = openConnection();
			Statement st = Objects.requireNonNull(connection).createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM data_mc_accounts WHERE uuid='" + uuid.toString() + "'");
			
			if (rs.next())
			{
				return true;
			}
		}
		catch (SQLException | ClassNotFoundException e)
		{
			logError(e);
		}
		return false;
	}
	
	public void updateName(UUID playerUUID, String newName)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement statement = Objects.requireNonNull(connection).createStatement();
					ResultSet results2 = statement.executeQuery("SELECT * FROM data_mc_accounts WHERE uuid = '" + playerUUID.toString() + "'");
					
					List<String> names = new ArrayList<>();
					while (results2.next())
					{
						String historyString = results2.getString("playername_history");
						String historyStringMinusBrackets = historyString.substring(1, historyString.length() - 1);
						String[] historyArray = historyStringMinusBrackets.split(", ");
						
						names = new ArrayList<>(Arrays.asList(historyArray));
					}
					results2.close();
					
					names.add(newName);
					
					statement.executeUpdate("UPDATE data_mc_accounts SET playername_history = '" + Arrays.toString(names.toArray()) + "', playername = '" + newName + "' WHERE uuid = '" + playerUUID.toString() + "';");
					statement.executeUpdate("UPDATE data_medieval_accounts SET medieval_name = '" + newName + "' WHERE medieval_name = '" + names.get(names.size() - 2) + "'");
					
					statement.close();
					connection.close();
					
					log("Aktualisierung des Spielernamens erfolgreich abgeschlossen.");
					updatePlayerDataStorage(names.get(names.size() - 2));
				}
				catch (Exception e)
				{
					logError(e);
				}
			}
		}.runTaskAsynchronously(get());
	}
	
	public void showPlayerInfo(Player sender, String target)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement st = Objects.requireNonNull(connection).createStatement();
					
					String target2 = target;
					
					if (Bukkit.getPlayer(target) != null)
					{
						target2 = Bukkit.getPlayer(target).getName();
					}
					
					String playername = null;
					String uuid = null;
					String id = null;
					String lastJoin = null;
					String firstJoin = null;
					
					
					ResultSet rs = st.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_name='" + target2 + "';");
					
					while (rs.next())
					{
						playername = rs.getString("medieval_name");
						lastJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("last_join")) + " Uhr";
						id = rs.getString("medieval_id");
						firstJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("first_join")) + " Uhr";
					}
					
					rs.close();
					
					ResultSet rs2 = st.executeQuery("SELECT * FROM data_link WHERE medieval_id='" + id + "';");
					
					while (rs2.next())
					{
						uuid = rs2.getString("uuid");
					}
					
					rs2.close();
					st.close();
					connection.close();
					
					if (id == null)
					{
						sendMessage(sender, getPrefix() + "Der angegebene Spieler konnte nicht gefunden werden.");
						return;
					}
					
					sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
					sendMessage(sender, "&6ID »&7 " + id);
					sendMessage(sender, "&6UUID »&7 " + uuid);
					sendMessage(sender, "&6Name »&7 " + playername);
					sendMessage(sender, "&6Letzter Serverbeitritt »&7 " + lastJoin);
					sendMessage(sender, "&6Erster Serverbeitritt »&7 " + firstJoin);
					/*
					TextComponent message = new TextComponent("[Klicke hier, um den Aktionslog des Spielers zu generieren]");
					message.setColor(ChatColor.GOLD);
					message.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/aktionslog " + id + " " + playername));
					message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Generiere den Aktionslog von " + playername).create()));
					
					sender.spigot().sendMessage(message);*/
					sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
				}
				catch (ClassNotFoundException | SQLException e)
				{
					logError(e);
				}
			}
		}.runTaskLaterAsynchronously(get(), 0);
	}
	
	public void createUserLogPaste(Player p, String id, String name)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement statement = Objects.requireNonNull(connection).createStatement();
					ResultSet results2 = statement.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_id='" + id + "';");
					
					List<String> ret = new ArrayList<>();
					while (results2.next())
					{
						String actionString = results2.getString("action_history");
						String actionStringMinusBrackets = actionString.substring(1, actionString.length() - 1);
						String[] actionArray = actionStringMinusBrackets.split(", ");
						
						ret = new ArrayList<>(Arrays.asList(actionArray));
					}
					
					results2.close();
					statement.close();
					connection.close();
					
					ImmutableList.Builder<String> builder = getPasteHeader(getDescription().getVersion(), name, ret.size());
					
					builder.add("```");
					for (String s : ret)
					{
						builder.add(s);
					}
					builder.add("```");
					
					ret.clear();
					
					Gist gist = Gist.builder()
									.description("MedievalData Actionlog")
									.file("medievaldata-actionlog.md", builder.build().stream().collect(Collectors.joining("\n")))
									.upload();
					
					sendMessage(p, getPrefix() + "Aktionslog URL:");
					sendMessage(p, "&7» " + gist.getUrl());
				}
				catch (Exception e)
				{
					logError(e);
				}
			}
		}.runTaskAsynchronously(get());
	}
	
	private ImmutableList.Builder<String> getPasteHeader(String version, String referenceUser, int size)
	{
		String date = DATE_FORMAT.format(new Date(System.currentTimeMillis()));
		
		return ImmutableList.<String>builder()
				   .add("## Aktionslog")
				   .add("#### Diese Datei wurde automatisch von [MedievalData](https://www.medievalsuite.de) v" + version + " generiert.")
				   .add("")
				   .add("### Metadaten")
				   .add("| Referenzierter Benutzer | Logeinträge | Zeitstempel |")
				   .add("|----------------|------|-------------|")
				   .add("| " + referenceUser + " | **" + size + "** | " + date + " |")
				   .add("")
				   .add("### Ausgabe");
	}
	
	private ImmutableList.Builder<String> getPasteHeaderError(String version, String referencePlugin, int size)
	{
		String date = DATE_FORMAT.format(new Date(System.currentTimeMillis()));
		
		return ImmutableList.<String>builder()
				   .add("## Errorlog")
				   .add("#### Diese Datei wurde automatisch von [MedievalData](https://www.medievalsuite.de) v" + version + " generiert.")
				   .add("")
				   .add("### Metadaten")
				   .add("| Referenziertes Plugin | Logeinträge | Zeitstempel |")
				   .add("|----------------|------|-------------|")
				   .add("| " + referencePlugin + " | **" + size + "** | " + date + " |")
				   .add("")
				   .add("### Ausgabe");
	}
	
	public void addActionLogEntry(String player, String medievalID, String entry)
	{
		String date = DATE_FORMAT.format(new Date(System.currentTimeMillis()));
		
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement st = Objects.requireNonNull(connection).createStatement();
					int id = 0;
					List<String> actionList = new ArrayList<>();
					
					ResultSet rs = st.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_id='" + medievalID + "';");
					
					String actionString = null;
					
					while (rs.next())
					{
						actionString = rs.getString("action_history");
						String actionStringMinusBrackets = actionString.substring(1, actionString.length() - 1);
						String[] actionArray = actionStringMinusBrackets.split(", ");
						id = actionArray.length + 1;
						actionList = new ArrayList<>(Arrays.asList(actionArray));
					}
					
					rs.close();
					
					Player p = Bukkit.getPlayer(player);
					
					if (actionString == null)
					{
						sendMessage(p, getPrefix() + "&cEs konnte kein Spieler mit der angegebenen ID gefunden werden");
						st.close();
						connection.close();
						return;
					}
					
					String message = "#" + id + " - " + date + " - [" + player + "] " + entry;
					actionList.add(message);
					
					String[] newActionArray = actionList.toArray(new String[actionList.size()]);
					
					st.executeUpdate("UPDATE data_medieval_accounts SET action_history='" + Arrays.toString(newActionArray) + "' WHERE medieval_id='" + medievalID + "';");
					
					st.close();
					connection.close();
					
					sendMessage(p, getPrefix() + "Der Logeintrag wurde erfolgreich erstellt");
				}
				catch (ClassNotFoundException | SQLException e)
				{
					logError(e);
				}
			}
		}.runTaskLaterAsynchronously(get(), 5);
		
	}
	
	public void finalizeUser(Player p)
	{
		get().playerDataStorage.remove(getID(p.getName()));
	}
	
	public void updateUser(int medievalID)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement statement = Objects.requireNonNull(connection).createStatement();
					Timestamp today = new Timestamp(new Date().getTime());
					statement.executeUpdate("UPDATE data_medieval_accounts SET last_join='" + today + "' WHERE medieval_id = " + medievalID);
					statement.close();
					connection.close();
					log("Datenbankeintrag wurde erfolgreich aktualisiert.");
					updatePlayerDataStorage(getName(medievalID));
				}
				catch (ClassNotFoundException | SQLException e)
				{
					logError(e);
				}
			}
		}.runTaskAsynchronously(get());
	}
	
	public void logError(Exception e)
	{
		List<StackTraceElement> ret = new ArrayList<>(Arrays.asList(e.getStackTrace()));
		ImmutableList.Builder<String> builder = getPasteHeaderError(getDescription().getVersion(), getDescription().getName(), ret.size());
		
		builder.add("```");
		for (StackTraceElement s : ret)
		{
			builder.add(s.toString());
		}
		builder.add("```");
		
		ret.clear();
		
		Gist gist = Gist.builder()
						.description("MedievalData Errorlog")
						.file("medievaldata-errorlog.md", builder.build().stream().collect(Collectors.joining("\n")))
						.upload();
		log(Level.WARNING, "Es ist ein Fehler aufgetreten: " + gist.getUrl());
	}
	
	private Connection openConnection() throws SQLException, ClassNotFoundException
	{/*
		if (connection != null && !connection.isClosed())
		{
			return null;
		}*/
		
		synchronized (this)
		{
			/*if (connection != null && !connection.isClosed())
			{
				return null;
			}*/
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?autoReconnect=true", this.username, this.password);
		}
	}
	
	
	public void createUser(UUID uuid, String playername, Boolean newUser)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = openConnection();
					Statement statement = Objects.requireNonNull(connection).createStatement();
					String[] arr = {playername};
					Timestamp today = new Timestamp(new Date().getTime());
					int id = 0;
					String date = DATE_FORMAT.format(new Date(System.currentTimeMillis()));
					String[] actionHistoryArray = new String[1];
					actionHistoryArray[0] = "#1 - " + date + " - [SYSTEM] Wurde in das System aufgenommen";
					statement.executeUpdate("INSERT INTO data_mc_accounts (uuid, playername, playername_history) VALUES ('" + uuid.toString() + "', '" + playername + "', '" + Arrays.toString(arr) + "')");
					statement.executeUpdate("INSERT INTO data_medieval_accounts (medieval_name, last_join, first_join, action_history) VALUES ('" + playername + "', '" + today + "', '2000-02-02 22:00:00', '" + Arrays.toString(actionHistoryArray) + "')");
					
					ResultSet results = statement.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_name = '" + playername + "';");
					
					while (results.next())
					{
						id = results.getInt("medieval_id");
					}
					
					statement.executeUpdate("INSERT INTO data_link (medieval_id, uuid, main) VALUES (" + id + ", '" + uuid.toString() + "', true)");
					
					if (newUser)
					{
						statement.executeUpdate("UPDATE data_medieval_accounts SET first_join='" + today + "' WHERE medieval_id = " + id + ";");
					}
					results.close();
					statement.close();
					connection.close();
					updatePlayers();
				}
				catch (ClassNotFoundException | SQLException e)
				{
					logError(e);
				}
			}
		}.runTaskAsynchronously(get());
	}
	
	private void updatePlayerDataStorage(String playername)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					Connection connection = get().openConnection();
					Statement st = Objects.requireNonNull(connection).createStatement();
					
					String playerName = null;
					String uuid = null;
					String id = null;
					String lastJoin = null;
					String firstJoin = null;
					String historyString = null;
					
					
					ResultSet rs = st.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_name='" + playername + "';");
					
					while (rs.next())
					{
						id = rs.getString("medieval_id");
						firstJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("first_join")) + " Uhr";
						playerName = rs.getString("medieval_name");
						lastJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("last_join")) + " Uhr";
					}
					
					rs.close();
					
					ResultSet rs2 = st.executeQuery("SELECT * FROM data_link WHERE medieval_id='" + id + "';");
					
					while (rs2.next())
					{
						uuid = rs2.getString("uuid");
					}
					
					rs2.close();
					
					ResultSet rs3 = st.executeQuery("SELECT * FROM data_mc_accounts WHERE uuid='" + uuid + "'");
					
					while (rs3.next())
					{
						historyString = rs3.getString("playername_history");
					}
					rs3.close();
					st.close();
					connection.close();
					
					if (id == null)
					{
						return;
					}
					
					String[] dataArray = {id, uuid, playerName, historyString, lastJoin, firstJoin};
					get().playerDataStorage.remove(Integer.valueOf(id));
					get().playerDataStorage.put(Integer.valueOf(id), dataArray);
				}
				catch (ClassNotFoundException | SQLException e)
				{
					logError(e);
				}
			}
		}.runTaskAsynchronously(get());
	}
	
	private void updatePlayers()
	{
		for (Player p : Bukkit.getOnlinePlayers())
		{
			new BukkitRunnable()
			{
				@Override
				public void run()
				{
					try
					{
						get().playerDataStorage.clear();
						String target = p.getName();
						Connection connection = get().openConnection();
						Statement st = Objects.requireNonNull(connection).createStatement();
						
						String playerName = null;
						String uuid = null;
						String id = null;
						String lastJoin = null;
						String firstJoin = null;
						String historyString = null;
						
						
						ResultSet rs = st.executeQuery("SELECT * FROM data_medieval_accounts WHERE medieval_name='" + target + "';");
						
						while (rs.next())
						{
							id = rs.getString("medieval_id");
							playerName = rs.getString("medieval_name");
							lastJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("last_join")) + " Uhr";
							firstJoin = new SimpleDateFormat("dd. MMMM yyyy HH:mm", Locale.GERMANY).format(rs.getTimestamp("first_join")) + " Uhr";
						}
						
						rs.close();
						
						ResultSet rs2 = st.executeQuery("SELECT * FROM data_link WHERE medieval_id='" + id + "';");
						
						while (rs2.next())
						{
							uuid = rs2.getString("uuid");
						}
						
						rs2.close();
						
						ResultSet rs3 = st.executeQuery("SELECT * FROM data_mc_accounts WHERE uuid='" + uuid + "'");
						
						while (rs3.next())
						{
							historyString = rs3.getString("playername_history");
						}
						rs3.close();
						st.close();
						connection.close();
						
						if (id == null)
						{
							return;
						}
						
						String[] dataArray = {id, uuid, playerName, historyString, lastJoin, firstJoin};
						
						get().playerDataStorage.put(Integer.valueOf(id), dataArray);
					}
					catch (ClassNotFoundException | SQLException e)
					{
						logError(e);
					}
				}
			}.runTaskAsynchronously(get());
		}
	}
	
	public void reloadConfig()
	{
		config();
	}
	
	@Override
	public YamlConfiguration getConfig()
	{
		return config;
	}
	
	private void config()
	{
		if (getDataFolder().mkdirs()) log("Pluginverzeichnis wurde erstellt.");
		this.f = new File(getDataFolder(), "config.yml");
		if (!this.f.exists())
		{
			try
			{
				Files.copy(getResource("config.yml"), this.f.toPath());
			}
			catch (Exception localException)
			{
				localException.printStackTrace();
			}
		}
		this.config = YamlConfiguration.loadConfiguration(this.f);
		if (this.config.getString("database.host") == null)
		{
			this.config.set("database.host", String.valueOf("localhost"));
		}
		if (this.config.getString("database.name") == null)
		{
			this.config.set("database.name", String.valueOf("MedievalData"));
		}
		if (this.config.getString("database.username") == null)
		{
			this.config.set("database.username", String.valueOf("MedievalData"));
		}
		if (this.config.getInt("database.port") == 0)
		{
			this.config.set("database.port", 3306);
		}
		if (this.config.getString("database.password") == null)
		{
			this.config.set("daabase.password", String.valueOf(""));
		}
		if (this.config.getString("enable auto updater") == null)
		{
			this.config.set("enable auto updater", Boolean.valueOf("true"));
		}
		sc();
		this.config = YamlConfiguration.loadConfiguration(this.f);
	}
	
	private void sc()
	{
		try
		{
			this.config.save(this.f);
		}
		catch (IOException e)
		{
			logError(e);
		}
	}
	
	public void sendMessage(CommandSender sender, String msg)
	{
		sender.sendMessage(msg.replaceAll("&", "\u00A7"));
	}
	
	public void sendMessage(String msg, String permission)
	{
		for (Player p : Bukkit.getOnlinePlayers())
		{
			if (p.hasPermission(permission))
			{
				sendMessage(p, msg);
			}
		}
	}
	
	private String getNamed(String msg, String pre, String post)
	{
		String[] s = msg.split(" ");
		StringBuilder sb = new StringBuilder();
		IntStream.range(0, s.length).forEach(i -> {
			Player p = Bukkit.getPlayer(s[i]);
			if (p != null)
			{
				if (p.getName().equalsIgnoreCase(s[i]))
				{
					String color = meta.getUserMeta(p, "color");
					s[i] = "§" + color + pre + p.getName() + post;
				}
			}
			if (i == s.length - 1)
			{
				sb.append(s[i]);
			}
			else
			{
				sb.append(s[i]).append(" ");
			}
		});
		return sb.toString();
	}
	
	public String getNamed(String msg)
	{
		return getNamed(msg, "", "§f");
	}
	
	public String getNamed(String msg, String post)
	{
		return getNamed(msg, "", post);
	}
}