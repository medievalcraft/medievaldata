/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MedievalPlugin extends JavaPlugin implements Listener
{
	// -------------------------------------------- //
	// LOAD
	// -------------------------------------------- //
	@Override
	public void onLoad()
	{
		this.onLoadPre();
		this.onLoadInner();
		this.onLoadPost();
	}
	
	private void onLoadPre()
	{
		this.logPrefixColored = "§8[§6" + this.getDescription().getPrefix() + "§8] §7";
		this.logPrefixPlain = ChatColor.stripColor(this.logPrefixColored);
	}
	
	private void onLoadInner()
	{
	
	}
	
	private void onLoadPost()
	{
	
	}
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnable()
	{
		if (!this.onEnablePre()) return;
		this.onEnableInner();
		this.onEnablePost();
	}
	
	private long enableTime;
	
	public long getEnableTime()
	{
		return this.enableTime;
	}
	
	private boolean onEnablePre()
	{
		this.enableTime = System.currentTimeMillis();
		
		log("=== STARTE PLUGIN ===");
		
		// Listener
		Bukkit.getPluginManager().registerEvents(this, this);
		
		return true;
	}
	
	public void onEnableInner()
	{
	}
	
	private void onEnablePost()
	{
		long ms = System.currentTimeMillis() - this.enableTime;
		log("=== START §2ABGESCHLOSSEN §r(Dauerte: " + ms + "ms) ===");
	}
	
	// -------------------------------------------- //
	// DISABLE
	// -------------------------------------------- //
	
	@Override
	public void onDisable()
	{
		log("Plugin gestoppt!");
	}
	
	// -------------------------------------------- //
	// CONVENIENCE
	// -------------------------------------------- //
	
	public void suicide()
	{
		this.log("Plugin wird gekillt!");
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
	// -------------------------------------------- //
	// LOGGING
	// -------------------------------------------- //
	
	private String logPrefixColored = null;
	private String logPrefixPlain = null;
	
	public void log(String msg)
	{
		log(Level.INFO, msg);
	}
	
	public void log(Level level, String msg)
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (level == Level.INFO && console != null)
		{
			console.sendMessage(this.logPrefixColored + msg);
		}
		else
		{
			Logger.getLogger("Minecraft").log(level, this.logPrefixPlain + msg);
		}
	}
	
	
}
