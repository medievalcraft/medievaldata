/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata.utils;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Represents a posted GitHub Gist
 */
public class Gist {
	private static final Gson GSON = new Gson();
	
	private static final MediaType JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
	
	private static final String GIST_API_URL = "https://api.github.com/gists";
	private static final String GIT_IO_URL = "https://git.io";
	
	public static Builder builder() {
		return new Builder();
	}
	
	private final String url;
	private final String id;
	
	private Gist(String url, String id) {
		this.url = url;
		this.id = id;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public String getId() {
		return this.id;
	}
	
	private static final class GistFile {
		private final String name;
		private final String content;
		
		private GistFile(String name, String content) {
			this.name = name;
			this.content = content;
		}
	}
	
	public static final class Builder {
		private final List<GistFile> files = new ArrayList<>();
		private boolean shorten = true;
		private String description = "MedievalData Gist";
		
		private Builder() {
		
		}
		
		public Builder file(String name, String content) {
			this.files.add(new GistFile(name, content));
			return this;
		}
		
		public Builder shorten(boolean shorten) {
			this.shorten = shorten;
			return this;
		}
		
		public Builder description(String description) {
			this.description = description;
			return this;
		}
		
		public Gist upload() {
			return Gist.upload(ImmutableList.copyOf(this.files), this.shorten, this.description);
		}
	}
	
	private static Gist upload(List<GistFile> files, boolean shorten, String description) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (JsonWriter jw = new JsonWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8))) {
			jw.beginObject();
			jw.name("description").value(description);
			jw.name("public").value(false);
			jw.name("files").beginObject();
			for (GistFile file : files) {
				jw.name(file.name).beginObject().name("content").value(file.content).endObject();
			}
			jw.endObject().endObject();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		RequestBody body = RequestBody.create(JSON_TYPE, out.toByteArray());
		String userpass = "Loapu:22a486216a26b2f4c3cee80f16d73b4e46f80e7d";
		String basicAuth = "Basic " + Base64.getEncoder().encodeToString((userpass).getBytes());
		Request request = new Request.Builder()
							  .url(GIST_API_URL)
							  .post(body)
							  .build();
		
		try (Response response = HttpClient.makeCall(request)) {
			try (ResponseBody responseBody = response.body()) {
				if (responseBody == null) {
					throw new RuntimeException("No response");
				}
				
				String id;
				String pasteUrl;
				try (InputStream inputStream = responseBody.byteStream()) {
					try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
						JsonObject object = GSON.fromJson(reader, JsonObject.class);
						id = object.get("id").getAsString();
						pasteUrl = object.get("html_url").getAsString();
					}
				}
				
				if (shorten) {
					pasteUrl = shortenUrl(pasteUrl);
				}
				
				return new Gist(pasteUrl, id);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private static String shortenUrl(String pasteUrl) {
		RequestBody requestBody = new FormBody.Builder()
									  .add("url", pasteUrl)
									  .build();
		
		Request request = new Request.Builder()
							  .url(GIT_IO_URL)
							  .post(requestBody)
							  .build();
		
		try (Response response = HttpClient.makeCall(request)) {
			String location = response.header("Location");
			if (location == null) {
				throw new RuntimeException("No location header");
			}
			return location;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pasteUrl;
	}
}