/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievaldata.utils;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

/**
 * Utilities for the OkHttp client
 */
public class HttpClient {
	
	private static final OkHttpClient CLIENT = new OkHttpClient.Builder()
												   .addInterceptor(new LuckPermsUserAgentInterceptor())
												   .build();
	
	public static Response makeCall(Request request) throws IOException {
		Response response = CLIENT.newCall(request).execute();
		if (!response.isSuccessful()) {
			throw exceptionForUnsuccessfulResponse(response);
		}
		return response;
	}
	
	private static RuntimeException exceptionForUnsuccessfulResponse(Response response) {
		String msg = "";
		try (ResponseBody responseBody = response.body()) {
			if (responseBody != null) {
				msg = responseBody.string();
			}
		} catch (IOException e) {
			// ignore
		}
		return new RuntimeException("Got response: " + response.code() + " - " + response.message() + " - " + msg);
	}
	
	private static final class LuckPermsUserAgentInterceptor implements Interceptor {
		@Override
		public Response intercept(Chain chain) throws IOException {
			Request orig = chain.request();
			Request modified = orig.newBuilder()
								   .header("User-Agent", "luckperms")
								   .build();
			
			return chain.proceed(modified);
		}
	}
	
	private HttpClient() {}
	
}